#pragma once
#include <iostream>
#include <iomanip>  

template <typename T>
class OwnStack
{
private:
    T* stack;
    int count; 

public:
    OwnStack()
    {
        stack = nullptr;
        count = 0;
    }
    OwnStack(const OwnStack& st)
    {
        try
        {
            stack = new T[st.count];
            count = st.count;
            for (auto i = 0; i < count; ++i)
                stack[i] = st.stack[i];
        }
        catch (std::bad_alloc & e)
        {
            std::cout << e.what() << std::endl;
        }
    }
    OwnStack& operator=(const OwnStack& st)
    {
        if (this == &st)
            return *this;

        if (count > 0)
        {
            count = 0;
            delete[] stack;
        }
        try
        {
            stack = new T[st.count];
            count = st.count;
            for (auto i = 0; i < count; ++i)
                stack[i] = st.stack[i];
        }
        catch (std::bad_alloc & e)
        {
            std::cout << e.what() << std::endl;
        }
        return *this;
    }
    ~OwnStack()
    {
        if (count > 0)
            delete[] stack;
    }

    void push(T item)
    {
        try
        {
            T* tmp = stack;
            stack = new T[count + 1];
            ++count;
            for (auto i = 0; i < count - 1; ++i)
                stack[i] = tmp[i];
            stack[count - 1] = item;
            if (count > 1)
                delete[] tmp;
        }
        catch (std::bad_alloc& e)
        {
            std::cout << e.what() << std::endl;
        }
    }
    T pop()
    {
        if (count == 0)
            return 0; 
        --count;
        return stack[count];
    }
    int Count() const
    {
        return count;
    }
    bool IsEmpty() const
    {
        return count == 0;
    }
    void Print() const
    {
	    T* p = stack;
        std::cout << "Stack: " << std::endl;
        if (count == 0)
            std::cout << "is empty." << std::endl;
        for (auto i = 0; i < count; ++i)
        {
            std::cout << "Item[" << i << "] = " << *p << std::endl;
            ++p; 
        }
        std::cout << std::endl;
    }
};