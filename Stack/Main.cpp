#include <cstdlib>
#include "OwnStack.h"
#include <future>
#include "CPUID.h"
#define RAND_GENERATE(X,Max) \
do\
{\
	X;\
}while (randNum > (Max));\

inline void print(std::string_view MyText)
{
	std::cout << MyText << std::endl;
}
unsigned generateData(bool supportRDRAND,unsigned maxValue=1024)
{
	unsigned randNum = 0;
	if (supportRDRAND)
	{
		RAND_GENERATE(_rdrand32_step(&randNum), maxValue)
	}
	else
	{
		RAND_GENERATE(randNum = rand(), maxValue)
	}

	return randNum;
}

int main()
{
	OwnStack<int> tmpStack;
	const bool isSupportRDRAND = InstructionSet::RDRAND();
	const auto addElements= 20;
	std::cout << "Add " << addElements << " to ownStack" << std::endl;
	std::cout << "Generating..." << std::endl;
	for (unsigned t=0;t<addElements;++t)
	{
		tmpStack.push(generateData(isSupportRDRAND));
	}
	if(tmpStack.Count()>2)
	{
		std::cout << "Get half by pop" << std::endl;
		const auto half = tmpStack.Count() / 2;
		for (int t = 0; t < half; ++t)
		{
			std::cout <<"Item["<<t<<"] = "<< tmpStack.pop()<<std::endl;
		}
		if (!tmpStack.IsEmpty())
		{
			std::cout << "Total elements left = " << tmpStack.Count() << std::endl;
			tmpStack.Print();
		}
	}
	system("pause");
		
	return EXIT_SUCCESS;
}
